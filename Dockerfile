FROM docker:27.3.1-dind-alpine3.20

ARG GCP_VERSION="494.0.0"
ARG GCP_URL="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli"

RUN apk update && \
    apk --no-cache upgrade && \
    apk add -q --no-cache \
        bash        \
        git         \
        gettext     \
        gnupg       \
        jq          \
        unzip       \
        wget        \
        gcc         \
        musl-dev    \
        aws-cli

RUN wget ${GCP_URL}-${GCP_VERSION}-linux-x86_64.tar.gz \
    -O /tmp/google-cloud-sdk.tar.gz | bash

RUN mkdir -p /usr/local/gcloud \
    && tar -C /usr/local/gcloud -xvzf /tmp/google-cloud-sdk.tar.gz \
    && /usr/local/gcloud/google-cloud-sdk/install.sh -q

ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin
